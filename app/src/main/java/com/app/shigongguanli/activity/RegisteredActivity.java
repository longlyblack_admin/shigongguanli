package com.app.shigongguanli.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.shigongguanli.HttpUrl;
import com.app.shigongguanli.R;
import com.app.shigongguanli.bean.BaseBean;
import com.google.gson.Gson;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

public class RegisteredActivity extends Activity {
    private EditText ed_phone,ed_name,ed_password,ed_gender,ed_idc,ed_tel,ed_idd,
            ed_birth,ed_nativ,ed_zm,ed_duty,ed_role,ed_bm,ed_address,ed_marriage,ed_work;
    private Button btn_registered;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registered);
        init();
    }

    private void init() {
        btn_registered=findViewById(R.id.btn_registered);
        ed_phone=findViewById(R.id.ed_phone);
        ed_name=findViewById(R.id.ed_name);
        ed_password=findViewById(R.id.ed_password);
        ed_gender=findViewById(R.id.ed_gender);
        ed_idc=findViewById(R.id.ed_idc);
        ed_tel=findViewById(R.id.ed_tel);
        ed_idd=findViewById(R.id.ed_idd);
        ed_birth=findViewById(R.id.ed_birth);
        ed_nativ=findViewById(R.id.ed_nativ);
        ed_zm=findViewById(R.id.ed_zm);
        ed_duty=findViewById(R.id.ed_duty);
        ed_role=findViewById(R.id.ed_role);
        ed_bm=findViewById(R.id.ed_bm);
        ed_address=findViewById(R.id.ed_address);
        ed_marriage=findViewById(R.id.ed_marriage);
        ed_work=findViewById(R.id.ed_work);
        btn_registered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO
                register();
            }
        });


    }

    private void register() {
        OkHttpUtils
                .post()
                .url(HttpUrl.REGISTER)
                .addParams("account",ed_name.getText().toString())
                .addParams("pwd",ed_password.getText().toString())
                .addParams("gender",ed_gender.getText().toString())
                .addParams("idc",ed_idc.getText().toString())
                .addParams("tel",ed_tel.getText().toString())
                .addParams("idd",ed_idd.getText().toString())
                .addParams("birth",ed_birth.getText().toString())
                .addParams("nativ",ed_nativ.getText().toString())
                .addParams("zm",ed_zm.getText().toString())
                .addParams("duty",ed_duty.getText().toString())
                .addParams("role",ed_role.getText().toString())
                .addParams("bm",ed_bm.getText().toString())
                .addParams("address",ed_address.getText().toString())
                .addParams("marriage",ed_marriage.getText().toString())
                .addParams("work",ed_work.getText().toString())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        e.printStackTrace();
                        Toast.makeText(RegisteredActivity.this,"请求失败",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Log.e("LoginActivity","response"+response);
                        BaseBean bean = new Gson().fromJson(response, BaseBean.class);
                        if(bean.getCode()==0){
                            Toast.makeText(RegisteredActivity.this,"注册成功",Toast.LENGTH_SHORT).show();
                            RegisteredActivity.this.finish();
                        }else{
                            Toast.makeText(RegisteredActivity.this,bean.getMsg(),Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

}