package com.app.shigongguanli.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.shigongguanli.HttpUrl;
import com.app.shigongguanli.R;
import com.app.shigongguanli.adapter.ShiGongAdapter;
import com.app.shigongguanli.bean.BaseBean;
import com.app.shigongguanli.bean.ShiGongBean;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.Call;

public class AddWeiXiuActivity extends BaseActivity {
    private EditText ed_name,ed_idc,ed_ream,ed_acc,ed_aacm,ed_fee,ed_foot;
    private Button btn_click;
    private ImageView iv_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_wei_xiu);
        init();
    }

    private void init() {
        iv_back=findViewById(R.id.iv_back);
        btn_click=findViewById(R.id.btn_click);
        ed_name=findViewById(R.id.ed_name);
        ed_idc=findViewById(R.id.ed_idc);
        ed_ream=findViewById(R.id.ed_ream);
        ed_acc=findViewById(R.id.ed_acc);
        ed_aacm=findViewById(R.id.ed_aacm);
        ed_fee=findViewById(R.id.ed_fee);
        ed_foot=findViewById(R.id.ed_foot);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddWeiXiuActivity.this.finish();
            }
        });
        btn_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日-HH:mm:ss");// HH:mm:ss
                //获取当前时间
                Date date = new Date(System.currentTimeMillis());
                Log.e("TAG","时间="+simpleDateFormat.format(date));
                send(simpleDateFormat.format(date));
            }
        });

    }
    private void send(String time){
        OkHttpUtils
                .post()
                .url(HttpUrl.AddWeiXiu)
                .addParams("name",ed_name.getText().toString())
                .addParams("idc",ed_idc.getText().toString())
                .addParams("ream",ed_ream.getText().toString())
                .addParams("acc",ed_acc.getText().toString())
                .addParams("aacm",ed_aacm.getText().toString())
                .addParams("fee",ed_fee.getText().toString())
                .addParams("foot",ed_foot.getText().toString())
                .addParams("wtime",time)

                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        e.printStackTrace();
                        Toast.makeText(AddWeiXiuActivity.this,"请求失败",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Log.e("TAG","response"+response);
                        BaseBean bean = new Gson().fromJson(response, BaseBean.class);
                        if(bean.getCode()==0){
                            Toast.makeText(AddWeiXiuActivity.this,"添加成功",Toast.LENGTH_SHORT).show();
                            AddWeiXiuActivity.this.finish();
                        }else{
                            Toast.makeText(AddWeiXiuActivity.this,"没有数据",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}