package com.app.shigongguanli.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.shigongguanli.HttpUrl;
import com.app.shigongguanli.R;
import com.app.shigongguanli.adapter.WarehouseAdapter;
import com.app.shigongguanli.bean.BaseBean;
import com.app.shigongguanli.bean.WarehouseBean;
import com.app.shigongguanli.listener.MyRVOnItemClickListener;
import com.google.gson.Gson;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.Call;

public class AddWarehouseActivity extends BaseActivity {
    private EditText ed_name,ed_type,ed_spec,ed_size,ed_num,ed_seat,ed_aname;
    private ImageView iv_back;
    private Button btn_add;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_warehouse);
        init();
    }

    private void init() {
        iv_back=findViewById(R.id.iv_back);
        btn_add=findViewById(R.id.btn_add);
        ed_name=findViewById(R.id.ed_name);
        ed_type=findViewById(R.id.ed_type);
        ed_spec=findViewById(R.id.ed_spec);
        ed_size=findViewById(R.id.ed_size);
        ed_num=findViewById(R.id.ed_num);
        ed_seat=findViewById(R.id.ed_seat);
        ed_aname=findViewById(R.id.ed_aname);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddWarehouseActivity.this.finish();
            }
        });
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日-HH:mm:ss");// HH:mm:ss
                //获取当前时间
                Date date = new Date(System.currentTimeMillis());
                Log.e("TAG","时间="+simpleDateFormat.format(date));
                addData(simpleDateFormat.format(date));
            }
        });
    }

    private void addData(String sj) {
        OkHttpUtils
                .post()
                .url(HttpUrl.ADDWAREHOUSE)
                .addParams("name",ed_name.getText().toString())
                .addParams("type",ed_type.getText().toString())
                .addParams("spec",ed_spec.getText().toString())
                .addParams("size",ed_size.getText().toString())
                .addParams("num",ed_num.getText().toString())
                .addParams("sj", sj)
                .addParams("seat",ed_seat.getText().toString())
                .addParams("aname",ed_aname.getText().toString())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        e.printStackTrace();
                        Toast.makeText(AddWarehouseActivity.this,"请求失败",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Log.e("LoginActivity","response"+response);
                        BaseBean bean = new Gson().fromJson(response, BaseBean.class);
                        if(bean.getCode()==0){
                            Toast.makeText(AddWarehouseActivity.this,"添加成功",Toast.LENGTH_SHORT).show();
                            AddWarehouseActivity.this.finish();
                        }else{
                            Toast.makeText(AddWarehouseActivity.this,bean.getMsg(),Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}