package com.app.shigongguanli.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.shigongguanli.HttpUrl;
import com.app.shigongguanli.MyApplication;
import com.app.shigongguanli.R;
import com.app.shigongguanli.bean.BaseBean;
import com.app.shigongguanli.bean.UserBean;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

public class LoginActivity extends Activity {
    TextView tvRegistered;
    EditText edPhone,edPassword;
    Button btnLogin;
    String tag="LoginActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }
    private void initView(){
        tvRegistered=findViewById(R.id.tv_registered);
        edPhone=findViewById(R.id.ed_phone);
        edPassword=findViewById(R.id.ed_password);
        btnLogin=findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                // startActivity(new Intent(LoginActivity.this, CSActivity.class));

                if(edPhone.getText().toString().length()<1){
                    Toast.makeText(LoginActivity.this,"请输入正确的帐号",Toast.LENGTH_SHORT).show();
                }else if(edPassword.getText().toString().length()<6){
                    Toast.makeText(LoginActivity.this,"请输入至少六位密码",Toast.LENGTH_SHORT).show();
                }else {
                    login(edPhone.getText().toString(),edPassword.getText().toString());
                }
            }
        });
        tvRegistered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisteredActivity.class));
            }
        });

    }
    private void login(final String name,String password){
        OkHttpUtils
                .get()
                .url(HttpUrl.LOGIN+"account="+name+"&pwd="+password)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        e.printStackTrace();
                        Toast.makeText(LoginActivity.this,"请求失败",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Log.e("LoginActivity","response"+response);
                        BaseBean bean = new Gson().fromJson(response, BaseBean.class);
                        if(bean.getCode()==0){
                            BaseBean<UserBean> integral = new Gson().fromJson(response, new TypeToken<BaseBean<UserBean>>() {
                            }.getType());
                            Toast.makeText(LoginActivity.this,"登录成功",Toast.LENGTH_SHORT).show();
                            MyApplication.user=integral.getData();

                            //
                            if(integral.getData().getRole().equals("1")){
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            }else{
                                startActivity(new Intent(LoginActivity.this,MainActivity.class));
                            }
                            LoginActivity.this.finish();
                        }else{
                            Toast.makeText(LoginActivity.this,bean.getMsg(),Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
