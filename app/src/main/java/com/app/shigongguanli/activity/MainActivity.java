package com.app.shigongguanli.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.app.shigongguanli.R;
import com.app.shigongguanli.activity.BaseActivity;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private Button btn_warehouse,btn_peijianshenbao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        btn_warehouse=findViewById(R.id.btn_warehouse);
        btn_peijianshenbao=findViewById(R.id.btn_peijianshenbao);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_warehouse://仓库
                startActivity(new Intent(MainActivity.this,WarehouseActivity.class));
                break;
            case R.id.btn_peijianshenbao://配件申报
                startActivity(new Intent(MainActivity.this,AccessoriesActivity.class));
                break;


        }
    }
}