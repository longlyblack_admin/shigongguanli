package com.app.shigongguanli.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.shigongguanli.HttpUrl;
import com.app.shigongguanli.MyApplication;
import com.app.shigongguanli.R;
import com.app.shigongguanli.adapter.WarehouseAdapter;
import com.app.shigongguanli.bean.BaseBean;
import com.app.shigongguanli.bean.UserBean;
import com.app.shigongguanli.bean.WarehouseBean;
import com.app.shigongguanli.listener.MyRVOnItemClickListener;
import com.app.shigongguanli.view.OutWarehouseDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import okhttp3.Call;

//仓库库存
public class WarehouseActivity extends BaseActivity {
    private ImageView iv_back;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private WarehouseAdapter adapter;
    private  List<WarehouseBean.Data> list;
    private Button btn_addWarehouse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warehouse);
        init();
    }

    private void init() {
        iv_back=findViewById(R.id.iv_back);
        btn_addWarehouse=findViewById(R.id.btn_addWarehouse);
        recyclerView=findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(WarehouseActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        btn_addWarehouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WarehouseActivity.this,AddWarehouseActivity.class));
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WarehouseActivity.this.finish();
            }
        });
        getWarehouseData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWarehouseData();
    }

    private void getWarehouseData(){
        OkHttpUtils
                .post()
                .url(HttpUrl.WAREHOUSELIST)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        e.printStackTrace();
                        Toast.makeText(WarehouseActivity.this,"请求失败",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Log.e("LoginActivity","response"+response);
                        BaseBean<WarehouseBean> bean = new Gson().fromJson(response, new TypeToken<BaseBean<WarehouseBean>>() {
                        }.getType());
                        if(bean.getData().getList()!=null){
                           list = bean.getData().getList();
                            adapter=new WarehouseAdapter(WarehouseActivity.this,list);
                            recyclerView.setAdapter(adapter);
                            adapter.setOnItemClick(new MyRVOnItemClickListener() {
                                @Override
                                public void onItemClick(int position) {
                                    //Toast.makeText(WarehouseActivity.this,"点击",Toast.LENGTH_SHORT).show();
                                    showMyDialog(list.get(position).getId());
                                }
                            });
                        }else{
                            Toast.makeText(WarehouseActivity.this,"没有数据",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    OutWarehouseDialog dialog;
    private void showMyDialog(final int id){
        if(dialog==null){
            dialog=new OutWarehouseDialog(WarehouseActivity.this);
        }
        dialog.show();
        dialog.setBtnClicklistener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog.getNumBer()!=null || dialog.getNumBer().length()>0){
                    send(id,dialog.getNumBer());
                    //dialog.dismiss();
                }
            }
        });

    }
    private void send(int id,String num){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日-HH:mm:ss");// HH:mm:ss
        //获取当前时间
        Date date = new Date(System.currentTimeMillis());
        Log.e("TAG","时间="+simpleDateFormat.format(date));
        OkHttpUtils
                .post()
                .url(HttpUrl.OUTWAREHOUSE)
                .addParams("id",id+"")
                .addParams("num",num)
                .addParams("sj",simpleDateFormat.format(date))
                .addParams("aname",MyApplication.user.getAccount())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        e.printStackTrace();
                        Toast.makeText(WarehouseActivity.this,"请求失败",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Log.e("LoginActivity","response"+response);
                        BaseBean bean = new Gson().fromJson(response, BaseBean.class);
                        if(bean.getCode()==0){
                            Toast.makeText(WarehouseActivity.this,"提交申请成功",Toast.LENGTH_SHORT).show();
                            if(dialog!=null){
                                dialog.dismiss();
                            }

                        }else{
                            Toast.makeText(WarehouseActivity.this,bean.getMsg(),Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}