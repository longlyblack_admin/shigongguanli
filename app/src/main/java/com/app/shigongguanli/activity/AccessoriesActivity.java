package com.app.shigongguanli.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.shigongguanli.HttpUrl;
import com.app.shigongguanli.R;
import com.app.shigongguanli.adapter.BaoYangAdapter;
import com.app.shigongguanli.adapter.MaintainAdapter;
import com.app.shigongguanli.adapter.ShiGongAdapter;
import com.app.shigongguanli.adapter.WarehouseAdapter;
import com.app.shigongguanli.bean.BaoYangBean;
import com.app.shigongguanli.bean.BaseBean;
import com.app.shigongguanli.bean.MaintainBean;
import com.app.shigongguanli.bean.ShiGongBean;
import com.app.shigongguanli.bean.WarehouseBean;
import com.app.shigongguanli.listener.MyRVOnItemClickListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

//配件申报
public class AccessoriesActivity extends BaseActivity implements View.OnClickListener {
    private TextView tv_weixiu,tv_baoyang,tv_shigong;
    private Button btn_weixiu,btn_baoyang,btn_shigong;
    private ImageView iv_back;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private MaintainAdapter maintainAdapter;//维修记录
    private BaoYangAdapter baoYangAdapter;//保养记录
    private ShiGongAdapter shiGongAdapter;//施工记录
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accessories);
        init();
    }

    private void init() {
        tv_weixiu=findViewById(R.id.tv_weixiu);
        tv_baoyang=findViewById(R.id.tv_baoyang);
        tv_shigong=findViewById(R.id.tv_shigong);
        btn_weixiu=findViewById(R.id.btn_weixiu);
        btn_baoyang=findViewById(R.id.btn_baoyang);
        btn_shigong=findViewById(R.id.btn_shigong);
        iv_back=findViewById(R.id.iv_back);
        recyclerView=findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(AccessoriesActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccessoriesActivity.this.finish();
            }
        });
        getWeixiuList();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_weixiu://维修记录
                getWeixiuList();
                tv_weixiu.setBackgroundColor(Color.parseColor("#4BBE97"));
                tv_baoyang.setBackgroundColor(Color.parseColor("#ffffff"));
                tv_shigong.setBackgroundColor(Color.parseColor("#ffffff"));
                break;
            case R.id.tv_baoyang://保养记录
                getBaoYangList();
                tv_weixiu.setBackgroundColor(Color.parseColor("#ffffff"));
                tv_baoyang.setBackgroundColor(Color.parseColor("#4BBE97"));
                tv_shigong.setBackgroundColor(Color.parseColor("#ffffff"));
                break;
            case R.id.tv_shigong://施工记录
                getShiGongList();
                tv_weixiu.setBackgroundColor(Color.parseColor("#ffffff"));
                tv_baoyang.setBackgroundColor(Color.parseColor("#ffffff"));
                tv_shigong.setBackgroundColor(Color.parseColor("#4BBE97"));
                break;
            case R.id.btn_weixiu://添加维修记录
                startActivity(new Intent(AccessoriesActivity.this,AddWeiXiuActivity.class));
                break;
            case R.id.btn_baoyang://添加保养记录
                startActivity(new Intent(AccessoriesActivity.this,AddWeiXiuActivity.class));
                break;
            case R.id.btn_shigong://添加施工记录
                startActivity(new Intent(AccessoriesActivity.this,AddWeiXiuActivity.class));
                break;
        }
    }
    private void getWeixiuList(){
        OkHttpUtils
                .post()
                .url(HttpUrl.WEIXIULIST)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        e.printStackTrace();
                        Toast.makeText(AccessoriesActivity.this,"请求失败",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Log.e("TAG","response"+response);
                        BaseBean<MaintainBean> bean = new Gson().fromJson(response, new TypeToken<BaseBean<MaintainBean>>() {
                        }.getType());
                        if(bean.getData().getList()!=null){
                            maintainAdapter=new MaintainAdapter(AccessoriesActivity.this,bean.getData().getList());
                            recyclerView.setAdapter(maintainAdapter);

                        }else{
                            Toast.makeText(AccessoriesActivity.this,"没有数据",Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }
    private void getBaoYangList(){
        OkHttpUtils
                .post()
                .url(HttpUrl.BAOYANGLIST)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        e.printStackTrace();
                        Toast.makeText(AccessoriesActivity.this,"请求失败",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Log.e("TAG","response"+response);
                        BaseBean<BaoYangBean> bean = new Gson().fromJson(response, new TypeToken<BaseBean<BaoYangBean>>() {
                        }.getType());
                        if(bean.getData().getList()!=null){
                            baoYangAdapter=new BaoYangAdapter(AccessoriesActivity.this,bean.getData().getList());
                            recyclerView.setAdapter(baoYangAdapter);

                        }else{
                            Toast.makeText(AccessoriesActivity.this,"没有数据",Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }
    private void getShiGongList(){
        OkHttpUtils
                .post()
                .url(HttpUrl.ShiGongLIST)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        e.printStackTrace();
                        Toast.makeText(AccessoriesActivity.this,"请求失败",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Log.e("TAG","response"+response);
                        BaseBean<ShiGongBean> bean = new Gson().fromJson(response, new TypeToken<BaseBean<ShiGongBean>>() {
                        }.getType());
                        if(bean.getData().getList()!=null){
                            shiGongAdapter=new ShiGongAdapter(AccessoriesActivity.this,bean.getData().getList());
                            recyclerView.setAdapter(shiGongAdapter);

                        }else{
                            Toast.makeText(AccessoriesActivity.this,"没有数据",Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }
}