package com.app.shigongguanli.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.shigongguanli.R;
import com.app.shigongguanli.bean.WarehouseBean;
import com.app.shigongguanli.listener.MyRVOnItemClickListener;
import com.bumptech.glide.Glide;

import java.util.List;

public class WarehouseAdapter extends RecyclerView.Adapter<WarehouseAdapter.MyViewHolder> {
    private List<WarehouseBean.Data> list;
    private LayoutInflater mInflater;
    private MyRVOnItemClickListener mOnItemClickLitener;
    private Context con;
    public WarehouseAdapter(Context context, List<WarehouseBean.Data> iconList){
        mInflater= LayoutInflater.from(context);
        list = iconList;
        this.con=context;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.from(viewGroup.getContext()).inflate(R.layout.warehouse_item,viewGroup,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, final int i) {
        WarehouseBean.Data data =list.get(i);
        viewHolder.tv_name.setText(data.getName());
        viewHolder.tv_type.setText(data.getType());
        viewHolder.tv_spec.setText(data.getSpec());
        viewHolder.tv_size.setText(data.getSize());
        viewHolder.tv_num.setText(data.getNum()+"");
        viewHolder.tv_sj.setText(data.getSj());
        viewHolder.tv_seat.setText(data.getSeat());
        viewHolder.tv_aname.setText(data.getAname());


        // viewHolder.iv_photo
        if (mOnItemClickLitener != null) {
            viewHolder.lin_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickLitener.onItemClick(i);
                }
            });

        }
    }
    public void setOnItemClick(MyRVOnItemClickListener mOnItemClickLitener){
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
    @Override
    public int getItemCount() {
        return list.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_name,tv_type,tv_spec,tv_size,tv_num,tv_sj,tv_seat,tv_aname;
        private LinearLayout lin_click;
        public MyViewHolder(View view){
            super(view);
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_type = (TextView) view.findViewById(R.id.tv_type);
            tv_spec = (TextView) view.findViewById(R.id.tv_spec);
            tv_size = (TextView) view.findViewById(R.id.tv_size);
            tv_num = (TextView) view.findViewById(R.id.tv_num);
            tv_sj = (TextView) view.findViewById(R.id.tv_sj);
            tv_seat = (TextView) view.findViewById(R.id.tv_seat);
            tv_aname = (TextView) view.findViewById(R.id.tv_aname);
            lin_click = (LinearLayout) view.findViewById(R.id.lin_click);
        }
    }

    public void setList(List<WarehouseBean.Data> l){
        list=l;
        this.notifyDataSetChanged();
    }
}

