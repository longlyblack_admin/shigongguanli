package com.app.shigongguanli.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.shigongguanli.R;
import com.app.shigongguanli.bean.BaoYangBean;
import com.app.shigongguanli.bean.MaintainBean;
import com.app.shigongguanli.listener.MyRVOnItemClickListener;

import java.util.List;

//保养记录
public class BaoYangAdapter extends RecyclerView.Adapter<BaoYangAdapter.MyViewHolder> {
    private List<BaoYangBean.Data> list;
    private LayoutInflater mInflater;
    private MyRVOnItemClickListener mOnItemClickLitener;
    private Context con;
    public BaoYangAdapter(Context context, List<BaoYangBean.Data> iconList){
        mInflater= LayoutInflater.from(context);
        list = iconList;
        this.con=context;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.from(viewGroup.getContext()).inflate(R.layout.baoyang_item,viewGroup,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, final int i) {
        BaoYangBean.Data data =list.get(i);
        viewHolder.tv_1.setText(data.getName());
        viewHolder.tv_2.setText(data.getSid()+"");
        viewHolder.tv_3.setText(data.getType());
        viewHolder.tv_4.setText(data.getWtime());
        viewHolder.tv_5.setText(data.getAcc()+"");
        viewHolder.tv_6.setText(data.getFee());
        viewHolder.tv_7.setText(data.getZj());


        // viewHolder.iv_photo
        if (mOnItemClickLitener != null) {
            viewHolder.lin_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickLitener.onItemClick(i);
                }
            });

        }
    }
    public void setOnItemClick(MyRVOnItemClickListener mOnItemClickLitener){
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
    @Override
    public int getItemCount() {
        return list.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_1,tv_2,tv_3,tv_4,tv_5,tv_6,tv_7;
        private LinearLayout lin_click;
        public MyViewHolder(View view){
            super(view);
            tv_1 = (TextView) view.findViewById(R.id.tv_1);
            tv_2 = (TextView) view.findViewById(R.id.tv_2);
            tv_3 = (TextView) view.findViewById(R.id.tv_3);
            tv_4 = (TextView) view.findViewById(R.id.tv_4);
            tv_5 = (TextView) view.findViewById(R.id.tv_5);
            tv_6 = (TextView) view.findViewById(R.id.tv_6);
            tv_7 = (TextView) view.findViewById(R.id.tv_7);
            lin_click = (LinearLayout) view.findViewById(R.id.lin_click);
        }
    }

    public void setList(List<BaoYangBean.Data> l){
        list=l;
        this.notifyDataSetChanged();
    }
}

