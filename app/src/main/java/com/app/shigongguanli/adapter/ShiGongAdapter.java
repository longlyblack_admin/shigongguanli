package com.app.shigongguanli.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.shigongguanli.R;
import com.app.shigongguanli.bean.BaoYangBean;
import com.app.shigongguanli.bean.ShiGongBean;
import com.app.shigongguanli.listener.MyRVOnItemClickListener;

import java.util.List;

//施工记录
public class ShiGongAdapter extends RecyclerView.Adapter<ShiGongAdapter.MyViewHolder> {
    private List<ShiGongBean.Data> list;
    private LayoutInflater mInflater;
    private MyRVOnItemClickListener mOnItemClickLitener;
    private Context con;
    public ShiGongAdapter(Context context, List<ShiGongBean.Data> iconList){
        mInflater= LayoutInflater.from(context);
        list = iconList;
        this.con=context;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.from(viewGroup.getContext()).inflate(R.layout.shigong_item,viewGroup,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, final int i) {
        ShiGongBean.Data data =list.get(i);
        viewHolder.tv_1.setText("施工人姓名:"+data.getName());
        viewHolder.tv_2.setText("施工人编号+"+data.getUid());
        viewHolder.tv_3.setText("施工地址:"+data.getAddress());
        viewHolder.tv_4.setText("工作内容:"+data.getContent());
        viewHolder.tv_5.setText("工作时间:"+data.getTim());
        viewHolder.tv_6.setText("台班类型:"+data.getWk());
        viewHolder.tv_7.setText("设备名称:"+data.getDname());
        viewHolder.tv_8.setText("设备编号:"+data.getIdd());
        viewHolder.tv_9.setText("运输路程:"+data.getTran());
        viewHolder.tv_10.setText("工作量:"+data.getWorkload());
        viewHolder.tv_11.setText("重机设备运行时长:"+data.getZtime());
        viewHolder.tv_12.setText("加油量:"+data.getOil());
        viewHolder.tv_13.setText("工作时长:"+data.getWtime());
        viewHolder.tv_14.setText("油料类型:"+data.getType());
        viewHolder.tv_15.setText("油料数量:"+data.getNum());
        viewHolder.tv_16.setText("状态:"+data.getRole());
        if(data.getZt().equals("1")){
            viewHolder.tv_17.setText("组长判断:同意");
        }else if(data.getZt().equals("2")){
            viewHolder.tv_17.setText("组长判断:不同意");
        }if(data.getZt().equals("0")){
            viewHolder.tv_17.setText("组长判断:未审核");
        }

        if(data.getGt().equals("1")){
            viewHolder.tv_18.setText("管理员判断:同意");
        }else if(data.getGt().equals("2")){
            viewHolder.tv_18.setText("管理员判断:不同意");
        }if(data.getGt().equals("0")){
            viewHolder.tv_18.setText("管理员判断:未审核");
        }

        if (mOnItemClickLitener != null) {
            viewHolder.lin_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickLitener.onItemClick(i);
                }
            });

        }
    }
    public void setOnItemClick(MyRVOnItemClickListener mOnItemClickLitener){
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
    @Override
    public int getItemCount() {
        return list.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_1,tv_2,tv_3,tv_4,tv_5,tv_6,tv_7,tv_8,tv_9,tv_10,tv_11,tv_12,tv_13,tv_14,tv_15,tv_16,tv_17,tv_18;
        private LinearLayout lin_click;
        public MyViewHolder(View view){
            super(view);
            tv_1 = (TextView) view.findViewById(R.id.tv_1);
            tv_2 = (TextView) view.findViewById(R.id.tv_2);
            tv_3 = (TextView) view.findViewById(R.id.tv_3);
            tv_4 = (TextView) view.findViewById(R.id.tv_4);
            tv_5 = (TextView) view.findViewById(R.id.tv_5);
            tv_6 = (TextView) view.findViewById(R.id.tv_6);
            tv_7 = (TextView) view.findViewById(R.id.tv_7);
            tv_8 = (TextView) view.findViewById(R.id.tv_8);
            tv_9 = (TextView) view.findViewById(R.id.tv_9);
            tv_10 = (TextView) view.findViewById(R.id.tv_10);
            tv_11 = (TextView) view.findViewById(R.id.tv_11);
            tv_12 = (TextView) view.findViewById(R.id.tv_12);
            tv_13 = (TextView) view.findViewById(R.id.tv_13);
            tv_14 = (TextView) view.findViewById(R.id.tv_14);
            tv_15 = (TextView) view.findViewById(R.id.tv_15);
            tv_16 = (TextView) view.findViewById(R.id.tv_16);
            tv_17 = (TextView) view.findViewById(R.id.tv_17);
            tv_18 = (TextView) view.findViewById(R.id.tv_18);
            lin_click = (LinearLayout) view.findViewById(R.id.lin_click);
        }
    }

    public void setList(List<ShiGongBean.Data> l){
        list=l;
        this.notifyDataSetChanged();
    }
}

