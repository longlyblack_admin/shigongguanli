package com.app.shigongguanli.bean;

import java.util.Date;

public class UserBean {
    /***
     *  "id": 1,
     *         "account": "张三",
     *         "pwd": "e10adc3949ba59abbe56e057f20f883e",
     *         "opwd": "e10adc3949ba59abbe56e057f20f883e",
     *         "tel": "123",
     *         "gender": "男",
     *         "idc": "1005640",
     *         "idd": "10054",
     *         "ctime": 1592636603000,
     *         "birth": "1986-05",
     *         "nativ": "北京市",
     *         "role": "1",
     *         "duty": "员工",
     *         "address": "南洋街5号",
     *         "marriage": "已婚",
     *         "work": "海岚大厦",
     *         "note": "无",
     *         "bm": "二队",
     *         "zm": "群众",
     *         "cql": null
     * */
    private int id;
    private String account;
    private String pwd;
    private String opwd;
    private String tel;
    private String gender;
    private String idc;
    private String idd;
    private long ctime;
    private String birth;
    private String nativ;
    private String role;
    private String duty;
    private String address;
    private String marriage;
    private String work;
    private String note;
    private String bm;
    private String zm;
    private String cql;

    @Override
    public String toString() {
        return "UserBean{" +
                "id=" + id +
                ", account='" + account + '\'' +
                ", pwd='" + pwd + '\'' +
                ", opwd='" + opwd + '\'' +
                ", tel='" + tel + '\'' +
                ", gender='" + gender + '\'' +
                ", idc='" + idc + '\'' +
                ", idd='" + idd + '\'' +
                ", ctime=" + ctime +
                ", birth='" + birth + '\'' +
                ", nativ='" + nativ + '\'' +
                ", role='" + role + '\'' +
                ", duty='" + duty + '\'' +
                ", address='" + address + '\'' +
                ", marriage='" + marriage + '\'' +
                ", work='" + work + '\'' +
                ", note='" + note + '\'' +
                ", bm='" + bm + '\'' +
                ", zm='" + zm + '\'' +
                ", cql='" + cql + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getOpwd() {
        return opwd;
    }

    public void setOpwd(String opwd) {
        this.opwd = opwd;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIdc() {
        return idc;
    }

    public void setIdc(String idc) {
        this.idc = idc;
    }

    public String getIdd() {
        return idd;
    }

    public void setIdd(String idd) {
        this.idd = idd;
    }

    public long getCtime() {
        return ctime;
    }

    public void setCtime(long ctime) {
        this.ctime = ctime;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getNativ() {
        return nativ;
    }

    public void setNativ(String nativ) {
        this.nativ = nativ;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMarriage() {
        return marriage;
    }

    public void setMarriage(String marriage) {
        this.marriage = marriage;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getBm() {
        return bm;
    }

    public void setBm(String bm) {
        this.bm = bm;
    }

    public String getZm() {
        return zm;
    }

    public void setZm(String zm) {
        this.zm = zm;
    }

    public String getCql() {
        return cql;
    }

    public void setCql(String cql) {
        this.cql = cql;
    }

    public UserBean(int id, String account, String pwd, String opwd, String tel, String gender, String idc, String idd, long ctime, String birth, String nativ, String role, String duty, String address, String marriage, String work, String note, String bm, String zm, String cql) {
        this.id = id;
        this.account = account;
        this.pwd = pwd;
        this.opwd = opwd;
        this.tel = tel;
        this.gender = gender;
        this.idc = idc;
        this.idd = idd;
        this.ctime = ctime;
        this.birth = birth;
        this.nativ = nativ;
        this.role = role;
        this.duty = duty;
        this.address = address;
        this.marriage = marriage;
        this.work = work;
        this.note = note;
        this.bm = bm;
        this.zm = zm;
        this.cql = cql;
    }
}
