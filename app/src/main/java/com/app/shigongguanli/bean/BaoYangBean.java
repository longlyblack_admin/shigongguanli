package com.app.shigongguanli.bean;

import java.util.List;

public class BaoYangBean {
    private List<Data> list;
    private PageBean pageBean;

    @Override
    public String toString() {
        return "BaoYangBean{" +
                "list=" + list +
                ", pageBean=" + pageBean +
                '}';
    }

    public BaoYangBean(List<Data> list, PageBean pageBean) {
        this.list = list;
        this.pageBean = pageBean;
    }

    public List<Data> getList() {
        return list;
    }

    public void setList(List<Data> list) {
        this.list = list;
    }

    public PageBean getPageBean() {
        return pageBean;
    }

    public void setPageBean(PageBean pageBean) {
        this.pageBean = pageBean;
    }

    public class PageBean{
        private int totalPageCount;
        private int pageSize;
        private int recordCount;
        private int currPageNo;
        private int startRow;
        private int endRow;

        @Override
        public String toString() {
            return "PageBean{" +
                    "totalPageCount=" + totalPageCount +
                    ", pageSize=" + pageSize +
                    ", recordCount=" + recordCount +
                    ", currPageNo=" + currPageNo +
                    ", startRow=" + startRow +
                    ", endRow=" + endRow +
                    '}';
        }

        public int getTotalPageCount() {
            return totalPageCount;
        }

        public void setTotalPageCount(int totalPageCount) {
            this.totalPageCount = totalPageCount;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getRecordCount() {
            return recordCount;
        }

        public void setRecordCount(int recordCount) {
            this.recordCount = recordCount;
        }

        public int getCurrPageNo() {
            return currPageNo;
        }

        public void setCurrPageNo(int currPageNo) {
            this.currPageNo = currPageNo;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public PageBean(int totalPageCount, int pageSize, int recordCount, int currPageNo, int startRow, int endRow) {
            this.totalPageCount = totalPageCount;
            this.pageSize = pageSize;
            this.recordCount = recordCount;
            this.currPageNo = currPageNo;
            this.startRow = startRow;
            this.endRow = endRow;
        }
    }
    public class Data{
        private int id;
        private String name;
        private int sid;
        private String type;
        private String wtime;
        private String acc;
        private String fee;
        private String zj;

        public Data(int id, String name, int sid, String type, String wtime, String acc, String fee, String zj) {
            this.id = id;
            this.name = name;
            this.sid = sid;
            this.type = type;
            this.wtime = wtime;
            this.acc = acc;
            this.fee = fee;
            this.zj = zj;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getSid() {
            return sid;
        }

        public void setSid(int sid) {
            this.sid = sid;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getWtime() {
            return wtime;
        }

        public void setWtime(String wtime) {
            this.wtime = wtime;
        }

        public String getAcc() {
            return acc;
        }

        public void setAcc(String acc) {
            this.acc = acc;
        }

        public String getFee() {
            return fee;
        }

        public void setFee(String fee) {
            this.fee = fee;
        }

        public String getZj() {
            return zj;
        }

        public void setZj(String zj) {
            this.zj = zj;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", sid=" + sid +
                    ", type='" + type + '\'' +
                    ", wtime='" + wtime + '\'' +
                    ", acc='" + acc + '\'' +
                    ", fee='" + fee + '\'' +
                    ", zj='" + zj + '\'' +
                    '}';
        }
    }
}
