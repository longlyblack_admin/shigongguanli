package com.app.shigongguanli.bean;

import java.util.List;

public class ShiGongBean {
    private List<Data> list;
    private PageBean pageBean;

    public List<Data> getList() {
        return list;
    }

    public void setList(List<Data> list) {
        this.list = list;
    }

    public PageBean getPageBean() {
        return pageBean;
    }

    public void setPageBean(PageBean pageBean) {
        this.pageBean = pageBean;
    }

    public class PageBean{
        private int totalPageCount;
        private int pageSize;
        private int recordCount;
        private int currPageNo;
        private int startRow;
        private int endRow;

        @Override
        public String toString() {
            return "PageBean{" +
                    "totalPageCount=" + totalPageCount +
                    ", pageSize=" + pageSize +
                    ", recordCount=" + recordCount +
                    ", currPageNo=" + currPageNo +
                    ", startRow=" + startRow +
                    ", endRow=" + endRow +
                    '}';
        }

        public int getTotalPageCount() {
            return totalPageCount;
        }

        public void setTotalPageCount(int totalPageCount) {
            this.totalPageCount = totalPageCount;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getRecordCount() {
            return recordCount;
        }

        public void setRecordCount(int recordCount) {
            this.recordCount = recordCount;
        }

        public int getCurrPageNo() {
            return currPageNo;
        }

        public void setCurrPageNo(int currPageNo) {
            this.currPageNo = currPageNo;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public PageBean(int totalPageCount, int pageSize, int recordCount, int currPageNo, int startRow, int endRow) {
            this.totalPageCount = totalPageCount;
            this.pageSize = pageSize;
            this.recordCount = recordCount;
            this.currPageNo = currPageNo;
            this.startRow = startRow;
            this.endRow = endRow;
        }
    }
    public class Data{
        private int id;
        private String name;
        private int uid;
        private String address;
        private String content;
        private String tim;
        private String wk;
        private String dname;
        private String idd;
        private String tran;
        private String workload;
        private String ztime;
        private String oil;
        private String wtime;
        private String type;
        private String num;
        private String role;
        private String zt;
        private String gt;

        public Data(int id, String name, int uid, String address, String content, String tim, String wk, String dname, String idd, String tran, String workload, String ztime, String oil, String wtime, String type, String num, String role, String zt, String gt) {
            this.id = id;
            this.name = name;
            this.uid = uid;
            this.address = address;
            this.content = content;
            this.tim = tim;
            this.wk = wk;
            this.dname = dname;
            this.idd = idd;
            this.tran = tran;
            this.workload = workload;
            this.ztime = ztime;
            this.oil = oil;
            this.wtime = wtime;
            this.type = type;
            this.num = num;
            this.role = role;
            this.zt = zt;
            this.gt = gt;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getTim() {
            return tim;
        }

        public void setTim(String tim) {
            this.tim = tim;
        }

        public String getWk() {
            return wk;
        }

        public void setWk(String wk) {
            this.wk = wk;
        }

        public String getDname() {
            return dname;
        }

        public void setDname(String dname) {
            this.dname = dname;
        }

        public String getIdd() {
            return idd;
        }

        public void setIdd(String idd) {
            this.idd = idd;
        }

        public String getTran() {
            return tran;
        }

        public void setTran(String tran) {
            this.tran = tran;
        }

        public String getWorkload() {
            return workload;
        }

        public void setWorkload(String workload) {
            this.workload = workload;
        }

        public String getZtime() {
            return ztime;
        }

        public void setZtime(String ztime) {
            this.ztime = ztime;
        }

        public String getOil() {
            return oil;
        }

        public void setOil(String oil) {
            this.oil = oil;
        }

        public String getWtime() {
            return wtime;
        }

        public void setWtime(String wtime) {
            this.wtime = wtime;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getZt() {
            return zt;
        }

        public void setZt(String zt) {
            this.zt = zt;
        }

        public String getGt() {
            return gt;
        }

        public void setGt(String gt) {
            this.gt = gt;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", uid=" + uid +
                    ", address='" + address + '\'' +
                    ", content='" + content + '\'' +
                    ", tim='" + tim + '\'' +
                    ", wk='" + wk + '\'' +
                    ", dname='" + dname + '\'' +
                    ", idd='" + idd + '\'' +
                    ", tran='" + tran + '\'' +
                    ", workload='" + workload + '\'' +
                    ", ztime='" + ztime + '\'' +
                    ", oil='" + oil + '\'' +
                    ", wtime='" + wtime + '\'' +
                    ", type='" + type + '\'' +
                    ", num='" + num + '\'' +
                    ", role='" + role + '\'' +
                    ", zt='" + zt + '\'' +
                    ", gt='" + gt + '\'' +
                    '}';
        }
    }
}
