package com.app.shigongguanli.bean;

import java.util.List;

public class WarehouseBean {
    private List<Data> list;
    private PageBean pageBean;

    @Override
    public String toString() {
        return "WarehouseBean{" +
                "list=" + list +
                ", pageBean=" + pageBean +
                '}';
    }

    public List<Data> getList() {
        return list;
    }

    public void setList(List<Data> list) {
        this.list = list;
    }

    public PageBean getPageBean() {
        return pageBean;
    }

    public void setPageBean(PageBean pageBean) {
        this.pageBean = pageBean;
    }

    public WarehouseBean(List<Data> list, PageBean pageBean) {
        this.list = list;
        this.pageBean = pageBean;
    }

    public class Data{
        private int id;
        private String name;
        private String type;
        private String spec;
        private String size;
        private int  num;
        private String sj;
        private String seat;
        private String aname;

        @Override
        public String toString() {
            return "Data{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", type='" + type + '\'' +
                    ", spec='" + spec + '\'' +
                    ", size='" + size + '\'' +
                    ", num=" + num +
                    ", sj='" + sj + '\'' +
                    ", seat='" + seat + '\'' +
                    ", aname='" + aname + '\'' +
                    '}';
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getSpec() {
            return spec;
        }

        public void setSpec(String spec) {
            this.spec = spec;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

        public String getSj() {
            return sj;
        }

        public void setSj(String sj) {
            this.sj = sj;
        }

        public String getSeat() {
            return seat;
        }

        public void setSeat(String seat) {
            this.seat = seat;
        }

        public String getAname() {
            return aname;
        }

        public void setAname(String aname) {
            this.aname = aname;
        }

        public Data(int id, String name, String type, String spec, String size, int num, String sj, String seat, String aname) {
            this.id = id;
            this.name = name;
            this.type = type;
            this.spec = spec;
            this.size = size;
            this.num = num;
            this.sj = sj;
            this.seat = seat;
            this.aname = aname;
        }
    }
    public class PageBean{
        private int totalPageCount;
        private int pageSize;
        private int recordCount;
        private int currPageNo;
        private int startRow;
        private int endRow;

        @Override
        public String toString() {
            return "PageBean{" +
                    "totalPageCount=" + totalPageCount +
                    ", pageSize=" + pageSize +
                    ", recordCount=" + recordCount +
                    ", currPageNo=" + currPageNo +
                    ", startRow=" + startRow +
                    ", endRow=" + endRow +
                    '}';
        }

        public int getTotalPageCount() {
            return totalPageCount;
        }

        public void setTotalPageCount(int totalPageCount) {
            this.totalPageCount = totalPageCount;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getRecordCount() {
            return recordCount;
        }

        public void setRecordCount(int recordCount) {
            this.recordCount = recordCount;
        }

        public int getCurrPageNo() {
            return currPageNo;
        }

        public void setCurrPageNo(int currPageNo) {
            this.currPageNo = currPageNo;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public PageBean(int totalPageCount, int pageSize, int recordCount, int currPageNo, int startRow, int endRow) {
            this.totalPageCount = totalPageCount;
            this.pageSize = pageSize;
            this.recordCount = recordCount;
            this.currPageNo = currPageNo;
            this.startRow = startRow;
            this.endRow = endRow;
        }
    }
}
