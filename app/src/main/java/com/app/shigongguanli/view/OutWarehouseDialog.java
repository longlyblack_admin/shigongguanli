package com.app.shigongguanli.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.app.shigongguanli.R;

public class OutWarehouseDialog extends Dialog {

    private Context context;
    private Button btn_send;
    private EditText ed_number;


    public OutWarehouseDialog(Context context) {
        super(context, R.style.dialog);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        init();
    }

    public void init() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.out_warehouse_dialog, null);
        setContentView(view);

        btn_send =view.findViewById(R.id.btn_send);
        ed_number =view.findViewById(R.id.ed_number);


        Window dialogWindow = getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics(); // 获取屏幕宽、高用
        lp.width = (int) (d.widthPixels * 0.8); // 高度设置为屏幕的0.6
        dialogWindow.setAttributes(lp);
    }

    public void setBtnClicklistener(View.OnClickListener clickListenerInterface) {
        btn_send.setOnClickListener(clickListenerInterface);

    }
    public String getNumBer(){
        return ed_number.getText().toString();
    }

}